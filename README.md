# Sprawozdanie z fizyki (ćwiczenie M1)

### Treść ćwiczenia
[Ćwiczenie M1](https://ftims.pg.edu.pl/documents/10673/46008532/cwiczenieM1.pdf)

### Kompilacja
`pdflatex fizyka.tex`

### Rezultat
[Plik fizyka.pdf](https://gitlab.com/marcel2012/fizyka-sprawozdanie-nr-1/-/jobs/artifacts/master/raw/fizyka.pdf?job=build)
